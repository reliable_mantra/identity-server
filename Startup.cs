﻿using System;
using System.IO;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.AspNetCore.DataProtection;
using System.Security.Cryptography.X509Certificates;
using IdentityServer4.Configuration;

namespace IdentityServer
{
    public class Startup
    {
        public IHostingEnvironment HostingEnvironment { get; private set; }
        public IConfiguration Configuration { get; private set; }
        private string HomePath { get; set; }
        private string CertPath { get; set; }
        private string CertPass { get; set; }

        public Startup(IConfiguration configuration, IHostingEnvironment env)
        {
            this.HostingEnvironment = env;
            this.Configuration = configuration;
            this.HomePath = System.Environment.GetFolderPath(System.Environment.SpecialFolder.Personal);
            this.CertPath = Configuration["Certificates:Signing"].Replace("~", HomePath);
            this.CertPass = Configuration["Certificates:SigningPass"];
        }

        // This method gets called by the runtime. Use this method to add services to the container.
        // For more information on how to configure your application, visit https://go.microsoft.com/fwlink/?LinkID=398940
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddMvc();

            string Cert = String.Concat(CertPath, "is.pfx");
            if (File.Exists(Cert))
            {
                services.AddDataProtection()
                    .PersistKeysToFileSystem(new DirectoryInfo(CertPath))
                    .ProtectKeysWithCertificate(new X509Certificate2(Cert, CertPass));
            }

            services.AddIdentityServer(options => {
                options.Authentication = new AuthenticationOptions()
                {
                    CookieLifetime = TimeSpan.FromMinutes(5),
                    CookieSlidingExpiration = true
                };
            })
                .LoadSigningCredentialFrom(CertPath, CertPass)
                .AddInMemoryIdentityResources(Config.GetIdentityResources())
                .AddInMemoryClients(Config.GetClients())
                .AddTestUsers(Config.GetUsers());
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseIdentityServer();

            app.UseStaticFiles();
            app.UseMvcWithDefaultRoute();
        }
    }
}
