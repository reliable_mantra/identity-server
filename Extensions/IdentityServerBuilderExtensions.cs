﻿using System;
using System.IO;
using System.Security.Cryptography.X509Certificates;
using Microsoft.Extensions.DependencyInjection;

namespace IdentityServer
{
    public static class IdentityServerBuilderExtensions
    {
        public static IIdentityServerBuilder LoadSigningCredentialFrom(this IIdentityServerBuilder builder, string path, string pass)
        {
            if (!string.IsNullOrEmpty(path) && File.Exists(path))
            {
                builder.AddSigningCredential(new X509Certificate2(String.Concat(path, "is.pfx"), pass));
            }
            else
            {
                builder.AddDeveloperSigningCredential();
            }

            return builder;
        }
    }
}
