FROM microsoft/dotnet:2.2-sdk AS build
WORKDIR /src

# copy csproj and restore dependencies
COPY *.csproj .
RUN dotnet restore

# copy everything and build the app
COPY . .
RUN dotnet publish -c Release -o /app

# setup the runtime
FROM microsoft/dotnet:2.2-aspnetcore-runtime AS runtime
WORKDIR /app
COPY --from=build /app .

ENV ASPNETCORE_URLS http://*:5000

ENTRYPOINT ["dotnet", "IdentityServer.dll"]
