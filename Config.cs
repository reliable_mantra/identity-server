﻿using IdentityServer4;
using IdentityServer4.Models;
using IdentityServer4.Test;
using System.Collections.Generic;
using System.Security.Claims;

namespace IdentityServer
{
    public static class Config
    {
        public static List<TestUser> GetUsers()
        {
            return new List<TestUser>
            {
                new TestUser
                {
                    SubjectId = "1",
                    Username = "alice",
                    Password = "password",

                    Claims = new []
                    {
                        new Claim("name", "Alice"),
                        new Claim("website", "https://alice.com")
                    }
                },
                new TestUser
                {
                    SubjectId = "2",
                    Username = "bob",
                    Password = "password",

                    Claims = new []
                    {
                        new Claim("name", "Bob"),
                        new Claim("website", "https://bob.com")
                    }
                }
            };
        }

        public static IEnumerable<IdentityResource> GetIdentityResources()
        {
            return new List<IdentityResource>
            {
                new IdentityResources.OpenId(),
                new IdentityResources.Profile(),
            };
        }

        public static IEnumerable<Client> GetClients()
        {
            return new List<Client>
            {

                // Client Credentials based grant type
                new Client
                {
                    ClientId = "client",
                    Enabled = true,

                    // no interactive user, use the clientid/secret for authentication
                    AllowedGrantTypes = GrantTypes.ClientCredentials,

                    // secret for authentication
                    ClientSecrets =
                    {
                        new Secret("secret".Sha256())
                    },

                    // scopes that client has access to
                    AllowedScopes = new List<string>
                    {
                        IdentityServerConstants.StandardScopes.OpenId,
                        IdentityServerConstants.StandardScopes.Profile
                    }
                },

                // Resource Owner (User) Password grant type
                new Client
                {
                    ClientId = "ro.client",
                    Enabled = true,
                    AllowedGrantTypes = GrantTypes.ResourceOwnerPassword,

                    ClientSecrets =
                    {
                        new Secret("secret".Sha256())
                    },
                    AllowedScopes = new List<string>
                    {
                        IdentityServerConstants.StandardScopes.OpenId,
                        IdentityServerConstants.StandardScopes.Profile
                    }
                },

                // Implicit grant type
                new Client
                {
                    ClientId = "implicit",
                    ClientName = "Implicit",
                    Enabled = true,
                    AllowedGrantTypes = GrantTypes.Implicit,
                
                    // where to redirect to after login
                    RedirectUris = { "http://localhost:8080/signin-callback" },

                    // where to redirect to after logout
                    PostLogoutRedirectUris = { "http://localhost:8080/signout-callback" },

                    AllowedScopes = new List<string>
                    {
                        IdentityServerConstants.StandardScopes.OpenId,
                        IdentityServerConstants.StandardScopes.Profile
                    }
                },

                // Authorization grant type
                new Client
                {
                    ClientId = "authorizationcode",
                    ClientName = "Authorization Code",
                    ClientSecrets =
                    {
                        new Secret("secret".Sha256())
                    },
                    Enabled = true,
                    AllowedGrantTypes = GrantTypes.Code,
                    RequireConsent = true,
                    AllowRememberConsent = false,
                
                    // where to redirect to after login
                    RedirectUris = { "http://localhost:8080/signin-callback" },

                    // where to redirect to after logout
                    PostLogoutRedirectUris = { "http://localhost:8080/signout-callback" },

                    AllowedScopes = new List<string>
                    {
                        IdentityServerConstants.StandardScopes.OpenId,
                        IdentityServerConstants.StandardScopes.Profile
                    },
                    AccessTokenType = AccessTokenType.Jwt
                }

            };
        }
    }
}
